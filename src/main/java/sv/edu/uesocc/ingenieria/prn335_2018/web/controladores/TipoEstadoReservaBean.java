/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.edu.uesocc.ingenieria.prn335_2018.web.controladores;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import sv.edu.uesocc.ingenieria.prn335_2018.datos.acceso.GenericMethods;
import sv.edu.uesocc.ingenieria.prn335_2018.datos.acceso.TipoEstadoReservaFacadeLocal;
import sv.edu.uesocc.ingenieria.prn335_2018.flota.datos.definicion.TipoEstadoReserva;

/**
 *
 * @author kevin Figueroa
 */
@Named(value = "backingBean")
@ViewScoped
public class TipoEstadoReservaBean extends BackingBean<TipoEstadoReserva> implements Serializable {

    @EJB
    TipoEstadoReservaFacadeLocal tipoestadoreservaFacade_;
    TipoEstadoReserva tipoEstadoReserva_;
    List<TipoEstadoReserva> Mylist = new ArrayList<>();

    private EstadosCRUD estado;

    TipoEstadoReserva data = new TipoEstadoReserva();

    public TipoEstadoReservaBean() {
    }

    private LazyDataModel<TipoEstadoReserva> lazyModelo;

    /**
     * metodo de inicializacion,
     */
    @PostConstruct
    public void init() {
        estado = EstadosCRUD.NONE;
        llenar();
        modelo();
    }

    /**
     * implementacion del LazydataModel
     */
    public void modelo() {
        tipoEstadoReserva_ = new TipoEstadoReserva();
        try {
            this.lazyModelo = new LazyDataModel<TipoEstadoReserva>() {

                @Override
                public Object getRowKey(TipoEstadoReserva object) {
                    if (object != null) {
                        return object.getIdTipoEstadoReserva();
                    }
                    return null;
                }

                @Override
                public TipoEstadoReserva getRowData(String rowKey) {
                    if (rowKey != null && !rowKey.isEmpty() && this.getWrappedData() != null) {
                        try {
                            for (TipoEstadoReserva ter : (List<TipoEstadoReserva>) getWrappedData()) {
                                Integer registro = new Integer(rowKey);
                                if (ter.getIdTipoEstadoReserva().compareTo(registro) == 0) {
                                    return ter;
                                }
                            }
                        } catch (Exception e) {
                            System.out.println("Excepcion" + e.getMessage());
                        }
                    }
                    return null;
                }

                @Override
                public List<TipoEstadoReserva> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {

                    try {
                        if (tipoestadoreservaFacade_ != null) {
                            this.setRowCount(tipoestadoreservaFacade_.count());
                            Mylist = tipoestadoreservaFacade_.findRange(first, pageSize);
                        }
                    } catch (Exception e) {
                        System.out.println("Excepcion" + e.getMessage());
                    }
                    return Mylist;
                }
            };

        } catch (Exception e) {
            System.out.println("Excepcion" + e.getMessage());
        }

    }

    /*
    metodos sobrecargados
     */
    @Override
    protected int getId() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected GenericMethods<TipoEstadoReserva> getFacadeLocal() {
        return tipoestadoreservaFacade_;
    }

    @Override
    protected TipoEstadoReserva getEntity() {
        return tipoEstadoReserva_;
    }

    public void onRowSelect(SelectEvent event) {
        estado = EstadosCRUD.EDITAR;
        tipoEstadoReserva_ = (TipoEstadoReserva) event.getObject();
    }

    @Override
    public void crear() {
        estado = EstadosCRUD.AGREGAR;
        super.crear();
        btncancelarHandler();
    }

    @Override
    public void modificar() {
        estado = EstadosCRUD.EDITAR;
        super.modificar();

        btncancelarHandler();
    }

    @Override
    public void eliminar() {
        estado = EstadosCRUD.ELIMINAR;
        super.eliminar();
        btncancelarHandler();

    }

    /**
     * metodo para reiniciar campos
     */
    public void btncancelarHandler() {
        estado = EstadosCRUD.NONE;
        tipoEstadoReserva_ = new TipoEstadoReserva();

    }

    /*
 * Getters y Setters-----
     */
    public TipoEstadoReserva getData() {
        return data;
    }

    public void setData(TipoEstadoReserva data) {
        this.data = data;
    }

    public TipoEstadoReservaFacadeLocal getTipoestadoreservaFacade_() {
        return tipoestadoreservaFacade_;
    }

    public void setTipoestadoreservaFacade_(TipoEstadoReservaFacadeLocal tipoestadoreservaFacade_) {
        this.tipoestadoreservaFacade_ = tipoestadoreservaFacade_;
    }

    public TipoEstadoReserva getTipoEstadoReserva_() {
        return tipoEstadoReserva_;
    }

    public void setTipoEstadoReserva_(TipoEstadoReserva tipoEstadoReserva_) {
        this.tipoEstadoReserva_ = tipoEstadoReserva_;
    }

    public List<TipoEstadoReserva> getMylist() {
        return Mylist;
    }

    public void setMylist(List<TipoEstadoReserva> Mylist) {
        this.Mylist = Mylist;
    }

    public LazyDataModel<TipoEstadoReserva> getLazyModelo() {
        return lazyModelo;
    }

    public void setLazyModelo(LazyDataModel<TipoEstadoReserva> lazyModelo) {
        this.lazyModelo = lazyModelo;
    }

    public List<TipoEstadoReserva> getLista() {
        return lista;
    }

    public void setLista(List<TipoEstadoReserva> lista) {
        this.lista = lista;
    }

    public LazyDataModel<TipoEstadoReserva> getLazyModel() {
        return lazyModel;
    }

    public void setLazyModel(LazyDataModel<TipoEstadoReserva> lazyModel) {
        this.lazyModel = lazyModel;
    }

    public EstadosCRUD getEstado() {
        return estado;
    }

}
